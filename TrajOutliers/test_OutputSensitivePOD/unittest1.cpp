#include "stdafx.h"
#include "CppUnitTest.h"
#include "pch.h"
#include "test_data_OutputSensitivePOD.h"

#include <iostream>
#include <vector>
#include <string>
#include <array>
#include <exception>
#include <iterator>
#include <numeric>
#include <cmath>

#include "OutputSensitivePOD.h"
#include "GeometryBackendTraits.h"
#include "OutlierDetectionTraits.h"
#include "IntervalTraits.h"
#include "NontransitivePOD.h"

//#include <tulib/HereProbeTraits.h>
//#include "tulib/HereTrajectoryTraits.h"
#include "LAMetroProbeTraits.h"
#include "LAMetroTrajectoryTraits.h"
#include "tulib/io/ProbeReader.h"
#include "tulib/io/GeoJSONUtils.h"
#include "tulib/TrajectoryReader.h"
#include "tulib/utils/text.h"
#include "tulib/utils/TrajectoryUtils.h"
#include "tulib/SortedProbeReader.h"

#include <GeographicLib/Geocentric.hpp>



using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace Microsoft
{
	namespace VisualStudio
	{
		namespace CppUnitTestFramework
		{
			template<> static std::wstring ToString<__int64>(const __int64& t) { RETURN_WIDE_STRING(t); }

			template<> static std::wstring ToString<long double>(const long double& t) { RETURN_WIDE_STRING(t); }

			//template<> static std::wstring ToString<size_t>(const size_t& t) { RETURN_WIDE_STRING(t); }
		}
	}
}

namespace test_TrajOutliers
{
	TEST_CLASS(test_OutputSensitivePOD)
	{
	public:

		TEST_METHOD(test_OSPOD)
		{
			using TrajectoryTraits = here::c2d::raw::TabularTrajectoryTraits;
			using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
			using GeometryTraits = GeometryKernel::TulibGeometryKernel;
			typedef GeometryTraits::NT NT;

			// Create trajectory reader
			std::unique_ptr<ProbeReader<ProbeTraits>> probe_reader;
			probe_reader = ProbeReaderFactory::create_from_string<ProbeTraits>(testdata::c2d_raw_csv);
			using ProbeInputIterator = decltype(probe_reader->begin());
			constexpr int PROBE_ID = ProbeTraits::ProbeColumns::PROBE_ID;
			SortedProbeReader<ProbeInputIterator, PROBE_ID> sorted_probe_reader(probe_reader->begin(), probe_reader->end());
			using SortedProbeInputIterator = decltype(sorted_probe_reader.begin());
			auto trajectory_reader = TrajectoryReader<TrajectoryTraits, SortedProbeInputIterator>(sorted_probe_reader.begin(), sorted_probe_reader.end());
			tulib_algorithms::OutputSensitivePOD<GeometryTraits, TrajectoryTraits, tulib_algorithms::DistanceOverTimeCheck<GeometryKernel::TulibGeometryKernel>> detect_outliers(21);
			std::vector<TrajectoryTraits::trajectory_type> filtered_trajectories;
			typedef std::vector<std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>> InputVector;
			typedef std::vector<InputVector::iterator> OutputVector;
			//tulib_core::MakePoint<GeometryTraits> make_point;
			for (auto trajectory : trajectory_reader)
			{
				//Create the input for our algorithm
				InputVector input;
				auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
				auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
				auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
				auto it_lon = lons.begin();
				auto it_t = times.begin();
				for (auto it_lat = lats.begin(); it_lat != lats.end(); it_lat++)
				{
					//std::array<GeometryTraits::NT, GeometryKernel::dimensions> p;
					//p = {*it_lat, *it_lon};
					//auto point = make_point(std::cbegin(p), std::cend(p)); //Make a point out of the probe so we can check distances using the geometry backend.
					auto ts = it_t->ts();
					auto tup = std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>(*it_lat, *it_lon, ts);
					input.push_back(tup);
					it_lon++;
					it_t++;
				}



				OutputVector result;
				//tulib_core::tulib_insert_iterator
				detect_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result));

				int size = result.size();
				int targetsize = 6;
				Assert::AreEqual(targetsize, size, 0);
			}
		}

	};

	TEST_CLASS(test_NontransitivePOD)
	{
		using TrajectoryTraits = here::c2d::raw::TabularTrajectoryTraits;
		using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
		using GeometryTraits = GeometryKernel::TulibGeometryKernel;
		typedef GeometryTraits::NT NT;

	public:
		TEST_METHOD(test_Merger)
		{
			using TrajectoryTraits = here::c2d::raw::TabularTrajectoryTraits;
			using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
			using GeometryTraits = GeometryKernel::TulibGeometryKernel;

			typedef GeometryTraits::NT NT;
			typedef SpeedIntervalTraits<NT> ITraits;
			typedef ITraits::Interval Interval;

			ITraits traits(0, 100, -20, 20);
			std::vector<Interval> input;
			input.push_back(Interval(0, 100, 1));
			input.push_back(Interval(1, 2, 2));
			input.push_back(Interval(6, 8, 2));
			input.push_back(Interval(30, 40, 2));
			input.push_back(Interval(50, 150, 3));
			input.push_back(Interval(120, 130, 4));
			input.push_back(Interval(200, 300, 5));
			input.push_back(Interval(400, 500, 5));
			input.push_back(Interval(250, 450, 6));
			input.push_back(Interval(200, 220, 7));
			std::vector<Interval> expected;
			expected.push_back(Interval(0, 50, 1));
			expected.push_back(Interval(50, 150, 3));
			expected.push_back(Interval(200, 250, 5));
			expected.push_back(Interval(250, 400, 6));
			expected.push_back(Interval(400, 500, 5));

			auto result = traits.merge(input);
			Assert::AreEqual(expected.size(), result.size());
			auto it_expected = expected.begin();
			ofstream myfile;
			//myfile.open("output.txt");
			//for (auto it = result.begin(); it != result.end(); it++)
			//	myfile << "(" << it->min << "," << it->max << "," << it->prev << ")\n";
			//myfile.close();
			for (auto it_result = result.begin(); it_result != result.end(); it_result++)
			{
				Assert::AreEqual(it_expected->max, it_result->max);
				Assert::AreEqual(it_expected->min, it_result->min);
				Assert::AreEqual(it_expected->prev, it_result->prev);
				it_expected++;
			}
	
		}
	
		TEST_METHOD(test_Propagate)
		{
			using TrajectoryTraits = here::c2d::raw::TabularTrajectoryTraits;
			using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
			using GeometryTraits = GeometryKernel::TulibGeometryKernel;

			typedef GeometryTraits::NT NT;
			typedef SpeedIntervalTraits<NT> ITraits;
			typedef ITraits::Interval Interval;

			//Test case where only going at max speed is viable
			ITraits traits1(0, 20, -4, 4);
			std::vector<Interval> input1;
			input1.push_back(Interval(0, 20, -1));
			auto result1 = traits1.propagate(input1, convert(tuple(0,0, 0)), convert(tuple(100,0, 5)), 1, 100);
			std::vector<Interval> expected1;
			expected1.push_back(Interval(20, 20, 1));
			Assert::AreEqual(expected1.size(), result1.size());
			auto e1_it = expected1.begin();
			for (auto r1_it = result1.begin(); r1_it != result1.end(); r1_it++)
			{
				Assert::AreEqual(e1_it->min, r1_it->min);
				Assert::AreEqual(e1_it->max, r1_it->max);
				Assert::AreEqual(e1_it->prev, r1_it->prev);
				e1_it++;
			}

			//Test case where the points are not consistent
			auto result2 = traits1.propagate(input1, tuple(0,0, 0), tuple(1000,0, 5), 1, 1000);
			std::vector<Interval> expected2;
			Assert::AreEqual(expected2.size(), result2.size());

			//Test case where two intervals "flip"
			std::vector<Interval> input3;
			input3.push_back(Interval(0, 0, 1));
			input3.push_back(Interval(20, 20, 2));
			auto result3 = traits1.propagate(input3, tuple(0,0, 0), tuple(50,0, 5), 3, 50);
			std::vector<Interval> expected3;
			expected3.push_back(Interval(20, 20, 3));
			expected3.push_back(Interval(0, 0, 3));
			Assert::AreEqual(expected3.size(), result3.size());
			auto e3_it = expected3.begin();
			for (auto r3_it = result3.begin(); r3_it != result3.end(); r3_it++)
			{
				Assert::AreEqual(e3_it->min, r3_it->min);
				Assert::AreEqual(e3_it->max, r3_it->max);
				Assert::AreEqual(e3_it->prev, r3_it->prev);
				e3_it++;
			}

			//Test case for propagation case 1
			ITraits traits4(15, 60, -3, 5);
			std::vector<Interval> input4;
			input4.push_back(Interval(30, 30, -1));
			auto result4 = traits4.propagate(input4, tuple(0,0, 0), tuple(148.5,0, 5) ,3, 148.5);
			std::vector<Interval> expected4;
			expected4.push_back(Interval(23, 23, 3));
			//Assert::AreEqual(300, 301);
			Assert::AreEqual(expected4.size(), result4.size());
			//Assert::AreEqual(666, 667);
			auto e4_it = expected4.begin();
			for (auto r4_it = result4.begin(); r4_it != result4.end(); r4_it++)
			{
				Assert::AreEqual(e4_it->min, r4_it->min);
				//Assert::AreEqual(e4_it->max, r4_it->max);
				//Assert::AreEqual(e4_it->prev, r4_it->prev);
				e4_it++;
			}

			//Test propagation case 2
			ITraits traits5(-30, 100, -2, 5);
			std::vector<Interval> input5;
			input5.push_back(Interval(25, 80, 4));
			auto result5 = traits5.propagate(input5, tuple(0,0, 0), tuple(1056,0, 11), 7, 1056);
			std::vector<Interval> expected5;
			expected5.push_back(Interval(96, 100, 7));
			Assert::AreEqual(expected5.size(), result5.size());
			auto e5_it = expected5.begin();
			for (auto r5_it = result5.begin(); r5_it != result5.end(); r5_it++)
			{
				Assert::AreEqual(e5_it->min, r5_it->min);
				Assert::AreEqual(e5_it->max, r5_it->max);
				Assert::AreEqual(e5_it->prev, r5_it->prev);
				e5_it++;
			}

			//Test propagation case 3 (case 1, but for max speed)
			ITraits traits6(-60, 100, -6, 8);
			std::vector<Interval> input6;
			input6.push_back(Interval(5, 12, 4));
			auto result6 = traits6.propagate(input6, tuple(187,0, 0), tuple(0,0, 10), 7, -187);
			std::vector<Interval> expected6;
			expected6.push_back(Interval(-60, -13, 7));
			Assert::AreEqual(expected6.size(), result6.size());
			auto e6_it = expected6.begin();
			for (auto r6_it = result6.begin(); r6_it != result6.end(); r6_it++)
			{
				//Assert::AreEqual(e6_it->min, r6_it->min);
				Assert::AreEqual(e6_it->max, r6_it->max);
				Assert::AreEqual(e6_it->prev, r6_it->prev);
				e6_it++;
			}

			//Test propagation case 4 (case 2, but for max speed)
			ITraits traits7(-69, 100, -10, 6);
			std::vector<Interval> input7;
			input7.push_back(Interval(-4, 10, 4));
			auto result7 = traits7.propagate(input7, tuple(0,0, 0), tuple(-607.5625,0, 12), 7, -607.5625);
			std::vector<Interval> expected7;
			expected7.push_back(Interval(-69, -58.5, 7));
			Assert::AreEqual(expected7.size(), result7.size());
			auto e7_it = expected7.begin();
			for (auto r7_it = result7.begin(); r7_it != result7.end(); r7_it++)
			{
				//Assert::AreEqual(e6_it->min, r6_it->min);
				Assert::AreEqual(e7_it->max, r7_it->max);
				Assert::AreEqual(e7_it->prev, r7_it->prev);
				e7_it++;
			}
		}

		TEST_METHOD(test_DP)
		{
			using TrajectoryTraits = here::c2d::raw::TabularTrajectoryTraits;
			using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
			using GeometryTraits = GeometryKernel::TulibGeometryKernel;

			typedef GeometryTraits::NT NT;
			typedef SpeedIntervalTraits<NT> ITraits;
			typedef ITraits::Interval Interval;
			typedef tuple<NT, NT, time_t> Probe;

			ITraits SITraits(0, 100, -1, 1);
			tulib_algorithms::NontransitivePOD<GeometryTraits, SpeedIntervalTraits<NT>> detect_cis_outliers(SITraits);
			typedef std::vector<std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>> NontransitiveInputVector;
			typedef std::vector<NontransitiveInputVector::iterator> NontransitiveOutputVector;
			NontransitiveInputVector input;
			NontransitiveOutputVector output;
			input.push_back(convert(Probe(0,0,0)));
			input.push_back(convert(Probe(487.5,0, 5)));
			input.push_back(convert(Probe(4512.5,0, 95)));
			input.push_back(convert(Probe(5000,0, 100)));
			std::vector<std::vector<int>> DPCount;
			//auto cou= convert(Probe(0, 0, 0));
			//auto cov = convert(Probe(487.5, 0, 5));
			//Assert::AreEqual(487.5, distance_exact(get<0>(cou), get<1>(cou), get<0>(cov), get<1>(cov)),0.001);
			detect_cis_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(output), DPCount);
			std::vector<Probe> filtered_points;
			for (auto it = input.begin(); it != input.end(); it++)
			{
				if (it == output.back()) //Because of backtracing in the algorithm, the output is processed back to front.
				{
					filtered_points.push_back(*it);
					output.pop_back();
				}
			}
			size_t expected = 3;
			Assert::AreEqual(expected,filtered_points.size());
		}

		std::tuple<NT, NT, time_t> convert(std::tuple<NT, NT, time_t> intup)
		{
			NT startlat = 38.2244301;
			NT startlon = 12.3456789;
			//Generate some test data
			LocalCoordinateReference<NT> ref(startlat, startlon);
			auto geo = ref.inverse(get<0>(intup), get<1>(intup));
			return tuple(get<0>(geo), get<1>(geo), get<2>(intup));
		}
	};
}