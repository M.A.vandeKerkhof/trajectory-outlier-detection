#ifndef TULIB_NONTRANSITIVESMARTGREEDYPOD_H
#define TULIB_NONTRANSITIVESMARTGREEDYPOD_H

#include "tulib/utils/Requirements.h"
#include "SmartGreedyPOD.h"
#include <algorithm>
#include <iostream>
#include <string>
namespace tulib_algorithms {
	template <class GeometryTraits, class IntervalTraits>
	class NontransitiveSmartGreedyPOD {
	private:
		typedef typename GeometryTraits::NT NT;
		IntervalTraits traits;

	public:
		NontransitiveSmartGreedyPOD(IntervalTraits InTraits) { traits = InTraits; }

		template <class InputIterator, class OutputIterator,
			typename = tulib_core::requires_random_access_iterator <InputIterator>,
			typename = tulib_core::requires_output_iterator <OutputIterator>,
			typename = tulib_core::requires_equality<typename InputIterator::value_type,
			typename OutputIterator::value_type::value_type >>
			void operator()(InputIterator first, InputIterator beyond, OutputIterator result)
		{
			map<tuple<__int64, __int64>, NT> distancemap;
			vector<vector<InputIterator>> trajectories;
			vector<vector<IntervalTraits::Interval>> currIntervals;
			vector<InputIterator> base;
			base.push_back(first);
			vector<IntervalTraits::Interval> firstInterval;
			firstInterval.push_back(traits.base_interval());
			currIntervals.push_back(firstInterval);
			trajectories.push_back(base);
			auto it = first;
			for (it; it != beyond; it++)
			{
				__int64 nextprobe_index = it - first;
				bool contained = false;
				auto interval_it = currIntervals.begin();
				for (auto traj_it = trajectories.begin(); traj_it != trajectories.end(); traj_it++)
				{
					__int64 prevprobe_index = (traj_it->back()) - first;
					auto newintervals = traits.propagate(*interval_it, *(traj_it->back()), *it, prevprobe_index, nextprobe_index, distancemap);
					if (newintervals.size() > 0)
					{
						traj_it->push_back(it);
						contained = true;
						*interval_it = newintervals;
					}
					interval_it++;
				}
				if (!contained)
				{
					vector<InputIterator> newtraj;
					newtraj.push_back(it);
					trajectories.push_back(newtraj);
					vector<IntervalTraits::Interval> newintervals;
					newintervals.push_back(traits.base_interval());
					currIntervals.push_back(newintervals);
				}
			}
			IterateComparer<vector<InputIterator>> comparer;
			vector<InputIterator> max = *(max_element(trajectories.begin(), trajectories.end(), comparer));
			auto it_m = max.begin();
			for (it_m; it_m != max.end(); it_m++)
			{
				*result = *it_m;
			}

		}
	};
}

#endif //TULIB_NONTRANSITIVESMARTGREEDYPOD_H

