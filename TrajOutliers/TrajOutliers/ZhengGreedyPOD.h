#ifndef TULIB_ZHENGPOD_H
#define TULIB_ZHENGPOD_H

#include "tulib/utils/Requirements.h"

using namespace std;
/*!
 * @brief a collection of algorithms in tulib
 */
namespace tulib_algorithms
{
	/*!
	* @tparam GeometryTraits
	* @tparam TrajectoryTraits
	* @tparam TestType
	*/
	template <class GeometryTraits, class TrajectoryTraits, class TestType>
	class ZhengGreedyPOD {


	private:
		typedef typename GeometryTraits::NT NT;
		typedef TestType TEST;
		typedef typename GeometryTraits::Tulib_Point Point;
		typedef typename TrajectoryTraits::ProbeTraits::ProbeColumns Columns;
		TEST test;

		NT threshold,min_seg_size;
		
	public:

		/*!
		*@param InThreshold
		*/
		ZhengGreedyPOD(NT InThreshold, NT InMinSegSize) { threshold = InThreshold; test = TEST(InThreshold); min_seg_size = InMinSegSize; };

		/*!
 *
 * @tparam InputIterator
 * @tparam OutputIterator
 * @param first
 * @param beyond
 * @param result
 */
		template <class InputIterator, class OutputIterator,
			typename = tulib_core::requires_random_access_iterator <InputIterator>,
			typename = tulib_core::requires_output_iterator <OutputIterator>,
			typename = tulib_core::requires_equality<typename InputIterator::value_type,
			typename OutputIterator::value_type::value_type >>
			void operator()(InputIterator first, InputIterator beyond, OutputIterator result)
		{
			vector<size_t> segments;
			size_t segmentsize = 1;
			auto it = first;
			auto prev = *it;
			it++;
			for (it; it != beyond; it++)
			{
				if (test(prev, *it))
					segmentsize++;
				else 
				{
					segments.push_back(segmentsize);
					segmentsize = 1;
				}
				prev = *it;
			}
			segments.push_back(segmentsize);
			it = first;
			for (auto segment : segments)
			{
				if (segment >= min_seg_size)
					for (size_t i = 0; i < segment; i++)
					{
						*result = it;
						it++;
					}
				else it = next(it, segment);
			}
		} //operator()
	}; //class GreedyZhengPOD
}

#endif //TULIB_ZHENGPOD_H
