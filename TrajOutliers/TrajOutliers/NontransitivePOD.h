#ifndef TULIB_NONTRANSITIVE_H
#define TULIB_NONTRANSITIVE_H

//#include "GeometryBackendTraits.h"
#include "tulib/utils/Requirements.h"
#include <algorithm>
#include <iostream>
#include <string>
namespace tulib_algorithms {
	template <class GeometryTraits, class IntervalTraits>
	class NontransitivePOD {
	private:
		typedef typename GeometryTraits::NT NT;
		IntervalTraits traits;

	public:
		NontransitivePOD(IntervalTraits InTraits) { traits = InTraits; }

		template <class InputIterator, class OutputIterator,
			typename = tulib_core::requires_random_access_iterator <InputIterator>,
			typename = tulib_core::requires_output_iterator <OutputIterator>,
			typename = tulib_core::requires_equality<typename InputIterator::value_type,
			typename OutputIterator::value_type::value_type >>
		size_t operator()(InputIterator first, InputIterator beyond, OutputIterator result)
		{
			//Declare our DP table
			std::vector<std::vector<std::vector<IntervalTraits::Interval>>> DP;
			size_t max_num_intervals = 0;

			//Tables for precomputing expensive values. Each probe has a vector containing the distances of all previous probes, this vector is in reverse direction, i.e. the preceding probe is the first entry.
			vector<vector<NT>> distances;
			vector<vector<NT>> min_const_sqrts;
			vector<vector<NT>> max_const_sqrts;
			for (auto fit = first; fit != beyond; fit++)
			{
				//Make new columns for the precompute table
				vector<NT> newdistcol;
				distances.push_back(newdistcol);
				vector<NT> newminsqrtcol;
				min_const_sqrts.push_back(newminsqrtcol);
				vector<NT> newmaxsqrtcol;
				max_const_sqrts.push_back(newmaxsqrtcol);
			}		

			size_t traj_size = beyond - first;

			//Handle the edge cases
			if (traj_size == 0)
			{
				return 0;
			}
			if (traj_size == 1)
			{
				*result = first;
				return 1;
			}

			size_t max_possible_k = traj_size;
			size_t lastProbeIndex = 0;
			auto lastInterval = traits.base_interval();
			for (size_t diagonal = 0; diagonal < traj_size+1; diagonal++) //traj_size + 1 because we need the final iteration to exit via break so lastInterval and lastProbeIndex get the correct value
			{
				//Test if we need to compute this diagonal
				bool done = false;
				max_possible_k = traj_size - diagonal; //Since we only add the diagonal after this check, max_possible_k is off by 1 at this point
				for (auto dit = DP.begin(); dit != DP.end(); dit++)
				{
					auto intervals = next(dit->begin(), max_possible_k);
					if (intervals->size() > 0)
					{
						done = true;
						//lastIntervals = *intervals;
						lastProbeIndex = (dit - DP.begin()) + max_possible_k;
						lastInterval = intervals->front();
						break;
					}
				}
				if (done)
				{
					break;
				}

				//Fill in D[i,1] for this diagonal
				vector<vector<IntervalTraits::Interval>> newdiagonal;
				vector<IntervalTraits::Interval> first_cell;
				first_cell.push_back(traits.base_interval());//D[i,1] is always the max interval
				newdiagonal.push_back(first_cell); 
				DP.push_back(newdiagonal);
				size_t k = 1;


				//Get iterators pointing to the correct columns of the precompute table
				auto distance_it = next(distances.begin(), diagonal); 
				auto min_sqrt_it = next(min_const_sqrts.begin(), diagonal);
				auto max_sqrt_it = next(max_const_sqrts.begin(), diagonal);

				vector<IntervalTraits::Interval> lastU;

				for (k; k < max_possible_k; k++) //Compute the new values for the diagonal
				{
					vector<IntervalTraits::Interval> U; //Union of propagated intervals
					//To compute the cell on row k for the new diagonal, we must propagate all intervals on row k-1 for all preceding diagonals.
					auto distance_sub_it = distance_it->begin();
					auto min_sqrt_sub_it = min_sqrt_it->begin();
					auto max_sqrt_sub_it = max_sqrt_it->begin();
					size_t col_index = DP.size() - 1;
					for (auto diag_it = DP.rbegin(); diag_it != DP.rend(); diag_it++)
					{
						auto prev_intervals = next(diag_it->begin(), k - 1);
						size_t prevprobe_index = k - 1 + col_index;
						size_t nextprobe_index = k + diagonal;
						auto prevprobe = next(first, prevprobe_index);
						auto nextprobe = next(first, nextprobe_index);
						//Check if we still have a precomputed value
						if (distance_sub_it == distance_it->end())
						{
							//Compute and store the new values
							NT distance = traits.distance(*prevprobe, *nextprobe);
							distance_it->push_back(distance);
							min_sqrt_it->push_back(traits.get_min_sqrt(*prevprobe, *nextprobe, distance));
							max_sqrt_it->push_back(traits.get_max_sqrt(*prevprobe, *nextprobe, distance));
							distance_sub_it = distance_it->end();
							distance_sub_it--;
							min_sqrt_sub_it = min_sqrt_it->end();
							min_sqrt_sub_it--;
							max_sqrt_sub_it = max_sqrt_it->end();
							max_sqrt_sub_it--;
						}
						auto propagated_intervals = traits.propagate(*prev_intervals, *prevprobe, *nextprobe, prevprobe_index, *distance_sub_it, *min_sqrt_sub_it, *max_sqrt_sub_it);
						U.insert(U.end(), propagated_intervals.begin(), propagated_intervals.end());
						if (U.size() > 1)
						{
							auto M = traits.merge(U);
							U = M;
						}
						//early out if our propagated interval has reached maximal size
						if ((U.size()) == 1 && U[0].min == traits.min_speed && U[0].max == traits.max_speed)
							break;

						distance_sub_it++;
						min_sqrt_sub_it++;
						max_sqrt_sub_it++;
						col_index--;
					}
					DP.back().push_back(U);
					if (U.size() > max_num_intervals)
						max_num_intervals = U.size();
					lastU = U;
					distance_it++;
					min_sqrt_it++;
					max_sqrt_it++;
				}
			}

			//After filling the table, use backtracking to get the filtered trajectory
			while (true)
			{
				//Parameter for dealing with rounding errors. If the difference between a number and some threshold is less than the slack, they are considered equal.
				NT slack = 0.001;
				*result = next(first, lastProbeIndex);
				size_t prevLastProbeIndex = lastProbeIndex;
				//time_t prevtime = std::get<2>(*next(first, lastProbeIndex));
				//cout << "prevtime" << endl;
				//cout << prevtime << endl;
				lastProbeIndex = lastInterval.prev;
				if (max_possible_k == 0)
					break;
				max_possible_k--;
				/*cout << "lastInterval" << endl;
				cout << lastInterval.min << " , " << lastInterval.max << endl;*/
				auto lpIntervals = DP.at(lastProbeIndex - max_possible_k).at(max_possible_k);
				/*cout << "lpIntervals: " << endl;
				for (auto inti : lpIntervals)
					cout << inti.min << " , " << inti.max << endl;*/
				bool check = false;
				auto diff = prevLastProbeIndex - lastProbeIndex - 1;
				auto distance_col = distances.at(prevLastProbeIndex - 1);
				auto dist = distance_col.at(diff);
				for (auto lpInt : lpIntervals)
				{
					vector<IntervalTraits::Interval> intervalvec;
					intervalvec.push_back(lpInt);
					auto fintervals = traits.propagate(intervalvec, *next(first, lastProbeIndex), *next(first, prevLastProbeIndex), lastProbeIndex, dist);
					for (auto fint : fintervals)
					{
						if (fint.min - lastInterval.min <= slack && lastInterval.max - fint.max <= slack)
						{
							lastInterval = lpInt;
							check = true;
						}
					}
					
				}
				if (check)
					continue;

				//auto frontintervals = traits.propagate(lpIntervals, *next(first, lastProbeIndex), *next(first, prevLastProbeIndex), lastProbeIndex, dist);
				//cout << "frontintervals:" << endl;
				//for (auto inti : frontintervals)
				//	cout << inti.min << "," << inti.max << endl;
				//for (auto fronti : frontintervals)
				//{
				//	auto fronttoback = traits.back_propagate(fronti, *next(first, lastProbeIndex), *next(first, prevLastProbeIndex), lastProbeIndex, dist, slack);
				//	cout << "fronttoback " << fronttoback.min << " , " << fronttoback.max << endl;
				//}
				vector<IntervalTraits::Interval> intervalvector;
				intervalvector.push_back(lastInterval);
				auto backinterval = traits.back_propagate(lastInterval, *next(first, lastProbeIndex), *next(first, prevLastProbeIndex), lastProbeIndex, dist,slack);
				/*cout << "backinterval" << endl;
				cout << backinterval.min << " , " << backinterval.max << endl;*/
				/*auto intvec = vector<IntervalTraits::Interval>();
				intvec.push_back(backinterval);
				auto backtofront = traits.propagate(intvec, *next(first, lastProbeIndex), *next(first, prevLastProbeIndex), lastProbeIndex, dist);
				for (auto iii : backtofront)
					cout << "backtofront: " << iii.min << " , " << iii.max << endl;*/
				
				for (auto interval : lpIntervals)
				{				
					//Check if there is overlap between this interval and the backpropagated one
					if (interval.max < backinterval.min || backinterval.max < interval.min)
					{
						continue;
					}

					NT intersectmin = max(interval.min, backinterval.min);
					NT intersectmax = min(interval.max, backinterval.max);
					lastInterval = IntervalTraits::Interval(intersectmin, intersectmax, interval.prev);
					check = true;
					break;
				}

				if (!check)
				{
					throw std::runtime_error("Backpropagate fail");
				}
			}
			return max_num_intervals;
		} //comp_outliers()
		
	};//Class NontransitivePOD
}

#endif //TULIB_NONTRANSITIVE_H
