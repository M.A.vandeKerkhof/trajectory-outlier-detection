
#ifndef TULIB_LAMETROPROBETRAITS_H
#define TULIB_LAMETROPROBETRAITS_H

#include "tulib/io/csv/ParseDate.h"
#include "tulib/io/csv/CategoricalField.h"
#include "tulib/io/csv/csv.h"
#include "tulib/ProbeTraits.h"

namespace here {
	namespace c2d {
		namespace raw {

			enum InputColumns {
				_PROBE_ID, _LAT, _LON, _SAMPLE_DATE
			};

			// Fields of interest: all
			enum ProbeColumns {
				PROBE_ID,  LAT, LON, SAMPLE_DATE,
			};

			class ProbeParseDate : public ParseDate {
			public:
				explicit ProbeParseDate(std::time_t ts = 0, string date_format = "%Y-%m-%d %H:%M:%S")
					:ParseDate(ts, std::move(date_format)) { }
			};

			class ProviderCategoricalField : public CategoricalField<std::string, ProviderCategoricalField> {};

			typedef csv<std::tuple<string, long double, long double, ProbeParseDate>,
				_PROBE_ID,  _LAT, _LON, _SAMPLE_DATE> ProbeCsv;

			typedef typename ProbeCsv::value_type ProbePoint;

			typedef _ProbeTraits<ProbeColumns, ProbeParseDate, ProbeCsv, ProbePoint> ProbeTraits;

		}  // namespace raw
	}  // namespace c2d
}  // namespace here

#endif //TULIB_LAMETROPROBETRAITS_H
