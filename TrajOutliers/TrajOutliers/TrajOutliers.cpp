// TrajOutliers.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

#include <vector>
#include <string>
#include "OutputSensitivePOD.h"
#include "GeometryBackendTraits.h"
#include "OutlierDetectionTraits.h"
#include "IntervalTraits.h"
#include "NontransitivePOD.h"
#include "NontransitiveGreedyPOD.h"
#include "NontransitiveSmartGreedyPOD.h"
#include "ZhengGreedyPOD.h"
#include "GreedyPOD.h"
#include "SmartGreedyPOD.h"
//#include "MainWindow.h"

#include "tulib/HereTrajectoryTraits.h"
#include <tulib/HereProbeTraits.h>
//#include "LAMetroTrajectoryTraits.h"
//#include "LAMetroProbeTraits.h"

#include "tulib/io/ProbeReader.h"
#include "tulib/io/GeoJSONUtils.h"
#include "tulib/TrajectoryReader.h"
#include "tulib/utils/text.h"
#include "tulib/utils/TrajectoryUtils.h"
#include "tulib/SortedProbeReader.h"
#include <boost/timer/timer.hpp>

//#include <QApplication>
//#include <QPushButton>
//#include <QFileDialog>

//Function for creating some test data like the data found in tulib/test_data.h
void CreateTestData() {
	using GeometryTraits = GeometryKernel::TulibGeometryKernel;
	typedef GeometryTraits::NT NT;
	string header = "\"PROBE_ID,SAMPLE_DATE,LAT,LON,HEADING,SPEED,PROBE_DATA_PROVIDER\\n\"";
	string probebegin = "\"5ba01fcb22000bf923550acf004d43d,2018-09-18 00:00:";
	string probeend = ",0,103.0,CONSUMER24\\n\"";
	int time = 0;
	NT startlat = 38.2244301;
	NT startlon = 12.3456789;
	//Generate some test data
	LocalCoordinateReference<NT> ref(startlat, startlon);
	//const GeographicLib::Geocentric& earth = GeographicLib::Geocentric::WGS84();
	std::vector<NT> x{ 0.0,100.0,200.0,300.0,400.0,500.0,600.0,700.0,799.0,900.0,1000.0 };
	std::vector<NT> y{ 0.0,10000.0,10000.0,0.0,0.0,-10000.0,-10000.0,0.0,0.0,0.0,0.0 };
	std::vector<std::array<NT, 2>> geocords;
	auto yit = y.begin();
	for (auto xit = x.begin(); xit != x.end(); xit++)
	{
		auto geo = ref.inverse(*xit, *yit);
		geocords.push_back(geo);
		yit++;
	}
	ofstream foofile;
	foofile.open("testdata.txt");

	foofile << header << "\n";

	for (auto it = geocords.begin(); it != geocords.end(); it++)
	{
		string empty = "";
		if (time < 10)
			empty = "0";
		foofile << probebegin << empty << time << "," << get<0>(*it) << "," << get<1>(*it) << probeend << "\n";
		time += 5;
	}
	foofile.close();
}

template <class GeometryTraits, class TrajectoryTraits, class NT,class TrajectoryType>
void OutputSensitiveExperiment(std::vector<TrajectoryType> trajectories, NT maxspeed, string const &outfilename, std::vector<TrajectoryType> &filtered_trajectories, bool gencsvs = false, bool genjsons = false)
{
	vector<string> stats;
	stringstream header;
	header << "TRAJECTORY_ID;NUM_PROBES_IN;NUM_PROBES_OUT;TIME_WALL;TIME_USER;TIME_SYS;" << endl;
	stats.push_back(header.str());
	vector<string> outliers;
	stringstream().swap(header);
	header << "TRAJECTORY_ID;NUM_OUTLIERS;OUTLIER0;OUTLIER1;...;" << endl;
	outliers.push_back(header.str());

	using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
	cout << "Running experiment" << endl;
	size_t trajcount = trajectories.size();
	cout << "Filtering " << trajcount << " trajectories" << endl;
	tulib_algorithms::OutputSensitivePOD<GeometryTraits, TrajectoryTraits, tulib_algorithms::DistanceOverTimeCheck<GeometryTraits>> detect_outliers(maxspeed);
	boost::timer::cpu_timer timer;

	typedef std::vector<std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>> InputVector;
	typedef std::vector<InputVector::iterator> OutputVector;
	size_t current_traj_index = 1;
	for (auto trajectory : trajectories)
	{
		cout << "Processing trajectory " << current_traj_index << " of " << trajcount << endl;
		current_traj_index++;

		auto id = trajectory.get<ProbeTraits::ProbeColumns::PROBE_ID>();
		auto traj_id = *(id.begin());
		InputVector input;
		auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
		auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
		auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
		auto it_lon = lons.begin();
		auto it_t = times.begin();
		for (auto it_lat = lats.begin(); it_lat != lats.end(); it_lat++)
		{
			auto tup = std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>(*it_lat, *it_lon, it_t->ts());
			input.push_back(tup);
			it_lon++;
			it_t++;
		}
		OutputVector result;
		timer.start();
		detect_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result));
		timer.stop();

		auto el = timer.elapsed();
		stringstream statsline;
		statsline << traj_id << ";" << id.size() << ";" << result.size() << ";" << el.wall << ";" << el.user << ";" << el.system << ";" << endl;
		stats.push_back(statsline.str());

		stringstream outliersline;
		outliersline << traj_id << ";" << (id.size() - result.size()) << ";";

		std::vector<ProbeTraits::ProbeCsv::value_type> filtered_points;
		auto tit = trajectory.begin();
		auto time_it = times.begin();
		for (auto it = input.begin(); it != input.end(); it++)
		{
			if (it == result.back()) //Because of backtracing in the algorithm, the output is processed back to front.
			{
				filtered_points.push_back(*tit);
				result.pop_back();
			}
			else
			{
				outliersline << *time_it << ";";
			}
			tit++;
			time_it++;
		}
		outliersline << endl;
		outliers.push_back(outliersline.str());


		TrajectoryType filtered_trajectory(filtered_points);
		stringstream filename;
		if (gencsvs)
		{
			// Create an output csv file

			filename << outfilename << "outcsvs/" << traj_id << ".csv";
			std::ofstream ofcsv(filename.str());
			ofcsv << filtered_trajectory;
			stringstream().swap(filename);
		}
		if (genjsons)
		{
			filename << outfilename << "outjsons/" << traj_id << ".geojson";
			// Create a GeoJSON for easy visualisation
			std::ofstream ofjson(filename.str());
			ofjson.setf(std::ios::fixed);
			vector<TrajectoryTraits::trajectory_type> jsonvector;
			jsonvector.push_back(filtered_trajectory);
			tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
				ProbeTraits::ProbeColumns::LAT,
				ProbeTraits::ProbeColumns::LON>(ofjson,
					jsonvector.begin(),
					std::prev(jsonvector.end()));
		}
		filtered_trajectories.push_back(filtered_trajectory);
	}

	stringstream statsfilename;
	statsfilename << outfilename << "stats.csv";
	ofstream statscsv(statsfilename.str());
	for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
		statscsv << *stats_it;
	stringstream outliersfilename;
	outliersfilename << outfilename << "outliers.csv";
	ofstream outlierscsv(outliersfilename.str());
	for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
		outlierscsv << *outliers_it;
	//	// Create an output csv file
	//	stringstream filename;
	//	filename << "outcsvs/" << traj_id << ".csv";
	//	std::ofstream ofcsv(filename.str());
	//	ofcsv << filtered_trajectory;

	//	stringstream().swap(filename);
	//	filename << "outjsons/" << traj_id << ".geojson";
	//	// Create a GeoJSON for easy visualisation
	//	std::ofstream ofjson(filename.str());
	//	ofjson.setf(std::ios::fixed);
	//	vector<TrajectoryTraits::trajectory_type> jsonvector;
	//	jsonvector.push_back(filtered_trajectory);
	//	tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
	//		ProbeTraits::ProbeColumns::LAT,
	//		ProbeTraits::ProbeColumns::LON>(ofjson,
	//			jsonvector.begin(),
	//			std::prev(jsonvector.end()));

	//	filtered_trajectories.push_back(filtered_trajectory);
	//}

	//ofstream statscsv("stats.csv");
	//for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
	//	statscsv << *stats_it;
	//ofstream outlierscsv("outliers.csv");
	//for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
	//	outlierscsv << *outliers_it;

	cout << "Experiment Completed" << endl;
}

template <class GeometryTraits,class TrajectoryTraits, class TrajectoryType, class NT>
void NontransitiveExperiment(std::vector<TrajectoryType> trajectories, NT minspeed, NT maxspeed, NT minacc, NT maxacc, NT detourfactor, string const &outfilename, std::vector<TrajectoryType> &filtered_trajectories, bool gencsvs = false, bool genjsons = false)
{
	using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
	SpeedIntervalTraits<NT> SITraits(minspeed,maxspeed,minacc,maxacc,detourfactor);
	tulib_algorithms::NontransitivePOD<GeometryTraits, SpeedIntervalTraits<NT>> detect_cis_outliers(SITraits);
	typedef std::vector<std::tuple<NT,NT, time_t>> NontransitiveInputVector;
	typedef std::vector<NontransitiveInputVector::iterator> NontransitiveOutputVector;
	std::vector<TrajectoryType> nontransitive_filtered_trajectories;

	cout << "Running experiment" << endl;
	size_t trajcount = trajectories.size();
	cout << "Filtering " << trajcount << " trajectories" << endl;
	boost::timer::cpu_timer timer;
	//Store our outputs until they are written to file
	vector<string> stats;
	stringstream header;
	header << "TRAJECTORY_ID;NUM_PROBES_IN;NUM_PROBES_OUT;TIME_WALL;TIME_USER;TIME_SYS;MAX_INTERVALS;" << endl;
	stats.push_back(header.str());
	vector<string> outliers;
	stringstream().swap(header);
	header << "TRAJECTORY_ID;NUM_OUTLIERS;OUTLIER0;OUTLIER1;...;" << endl;
	outliers.push_back(header.str());
	//Open a file to write our results in
	//ofstream outfile;
	//outfile.open(outfilename);
	//outfile << "Nontransitive Outlier Experiment" << endl;
	//outfile << "MINSPEED" << endl << minspeed << endl << "MAXSPEED"<<endl << maxspeed << endl << "MINACC"<< endl << minacc << endl << "MAXACC"<< endl << maxacc << endl << "DETOURFACTOR" << endl << detourfactor << endl << endl;
	size_t current_traj_index = 1;

	for (auto trajectory : trajectories)
	{
		cout << "Processing trajectory " << current_traj_index << " of " << trajcount << endl;
		current_traj_index++;
		NontransitiveInputVector input;
		//outfile << "TRAJECTORY" << endl;
		auto id = trajectory.get<ProbeTraits::ProbeColumns::PROBE_ID>();
		//cout << "trajectory has " << id.size() << " columns." << endl;
		auto traj_id = *id.begin();

		//outfile << *id.begin() << endl;
		//outfile << "NUM_PROBES_PRE" << endl;
		//outfile << id.size() << endl;
		//outfile << "TIME" << endl;

		auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
		auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
		auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
		auto it_t = times.begin();
		auto it_lon = lons.begin();
		
		for (auto it_lat = lats.begin(); it_lat != lats.end(); it_lat++)
		{
			cout << *it_t << " , " << it_t->ts() << endl;
			//if (*it_t == eyy)
			//	cout << "found it!" << endl;
			input.push_back(tuple<NT,NT, time_t>(*it_lat, *it_lon, it_t->ts()));
			it_t++;
			it_lon++;
		}

		NontransitiveOutputVector result;
		//cout << "trajectory has size " << trajectory.size() << endl;
		//cout << "input has size " << input.size() << endl;
		//cout << "treating it as one of size " << input.end() - input.begin() << endl;
		//vector<vector<int>> DPCount;
		timer.start();
		//detect_cis_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result),DPCount);
		size_t maxDPcellval = detect_cis_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result));
		/*timer.stop();
		std::stringstream ss;
		ss << "DPtables/" << traj_id << "DPtable.csv";
		ofstream dpcsv(ss.str());
		int maxDPcellval = -1;
		for (auto dit = DPCount.begin(); dit != DPCount.end(); dit++)
		{
			for (auto cit = dit->begin(); cit != dit->end(); cit++)
			{
				if (*cit > maxDPcellval)
					maxDPcellval = *cit;
				dpcsv << *cit << ",";
			}
			dpcsv << endl;
		}*/
		auto el = timer.elapsed();
		stringstream statsline;
		statsline << traj_id << ";" << id.size() << ";" << result.size() << ";" << el.wall << ";" << el.user << ";" << el.system << ";" << maxDPcellval << ";" << endl;
		stats.push_back(statsline.str());
		//outfile << "wall " << el.wall << " user " << el.user << " system " << el.system << endl;
		//outfile << "NUM_PROBES_POST" << endl;
		//outfile << result.size() << endl;
		//outfile << "OUTLIERS" << endl;
		stringstream outliersline;
		outliersline << traj_id << ";" << (id.size() - result.size()) << ";";
		//Match the returned indices back to the full probes
		auto tit = trajectory.begin();
		std::vector<ProbeTraits::ProbeCsv::value_type> filtered_points;
		auto time_it = times.begin();
		for (auto it = input.begin(); it != input.end(); it++)
		{
			if (it == result.back()) //Because of backtracing in the algorithm, the output is processed back to front.
			{
				filtered_points.push_back(*tit);
				result.pop_back();
			}
			else 
			{
				//outfile << *time_it << endl;
				outliersline << *time_it << ";";
			}
			tit++;
			time_it++;
		}
		outliersline << endl;
		outliers.push_back(outliersline.str());
		TrajectoryType filtered_trajectory(filtered_points);

		stringstream filename;
		if (gencsvs)
		{
			// Create an output csv file

			filename << outfilename << "outcsvs/" << traj_id << ".csv";
			std::ofstream ofcsv(filename.str());
			ofcsv << filtered_trajectory;
			stringstream().swap(filename);
		}
		if (genjsons)
		{
			filename << outfilename << "outjsons/" << traj_id << ".geojson";
			// Create a GeoJSON for easy visualisation
			std::ofstream ofjson(filename.str());
			ofjson.setf(std::ios::fixed);
			vector<TrajectoryTraits::trajectory_type> jsonvector;
			jsonvector.push_back(filtered_trajectory);
			tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
				ProbeTraits::ProbeColumns::LAT,
				ProbeTraits::ProbeColumns::LON>(ofjson,
					jsonvector.begin(),
					std::prev(jsonvector.end()));
		}
		filtered_trajectories.push_back(filtered_trajectory);
		//if (current_traj_index > 10)
		//	break;
	}

	stringstream statsfilename;
	statsfilename << outfilename << "stats.csv";
	ofstream statscsv(statsfilename.str());
	for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
		statscsv << *stats_it;
	stringstream outliersfilename;
	outliersfilename << outfilename << "outliers.csv";
	ofstream outlierscsv(outliersfilename.str());
	for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
		outlierscsv << *outliers_it;
	cout << "Experiment completed" << endl;
	//	// Create an output csv file
	//	stringstream filename;
	//	filename << "outcsvs/" << traj_id << ".csv";
	//	std::ofstream ofcsv(filename.str());
	//		ofcsv << newtraj;

	//	stringstream().swap(filename);
	//	filename << "outjsons/" << traj_id << ".geojson";
	//	// Create a GeoJSON for easy visualisation
	//	std::ofstream ofjson(filename.str());
	//	ofjson.setf(std::ios::fixed);
	//	vector<TrajectoryTraits::trajectory_type> jsonvector;
	//	jsonvector.push_back(newtraj);
	//	tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
	//		ProbeTraits::ProbeColumns::LAT,
	//		ProbeTraits::ProbeColumns::LON>(ofjson,
	//			jsonvector.begin(),
	//			std::prev(jsonvector.end()));

	//	filtered_trajectories.push_back(newtraj);
	//	if (current_traj_index > 10)
	//	break;
	//}
	//ofstream statscsv("stats.csv");
	//for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
	//	statscsv << *stats_it;
	//ofstream outlierscsv("outliers.csv");
	//for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
	//	outlierscsv << *outliers_it;
}

template <class GeometryTraits, class TrajectoryTraits, class NT, class TrajectoryType>
void ZhengGreedyExperiment(std::vector<TrajectoryType> trajectories, NT maxspeed, NT min_seg_size, string const&outfilename, std::vector<TrajectoryType> &filtered_trajectories, bool gencsvs = false, bool genjsons = false)
{
	vector<string> stats;
	stringstream header;
	header << "TRAJECTORY_ID;NUM_PROBES_IN;NUM_PROBES_OUT;TIME_WALL;TIME_USER;TIME_SYS;" << endl;
	stats.push_back(header.str());
	vector<string> outliers;
	stringstream().swap(header);
	header << "TRAJECTORY_ID;NUM_OUTLIERS;OUTLIER0;OUTLIER1;...;" << endl;
	outliers.push_back(header.str());

	using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
	cout << "Running experiment" << endl;
	size_t trajcount = trajectories.size();
	cout << "Filtering " << trajcount << " trajectories" << endl;
	tulib_algorithms::ZhengGreedyPOD<GeometryTraits, TrajectoryTraits, tulib_algorithms::DistanceOverTimeCheck<GeometryTraits>> detect_outliers(maxspeed, min_seg_size);
	//tulib_algorithms::OutputSensitivePOD<GeometryTraits, TrajectoryTraits, tulib_algorithms::DistanceOverTimeCheck<GeometryTraits>> detect_outliers(maxspeed);
	boost::timer::cpu_timer timer;


	typedef std::vector<std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>> InputVector;
	typedef std::vector<InputVector::iterator> OutputVector;
	size_t current_traj_index = 1;
	for (auto trajectory : trajectories)
	{
		cout << "Processing trajectory " << current_traj_index << " of " << trajcount << endl;
		current_traj_index++;

		auto id = trajectory.get<ProbeTraits::ProbeColumns::PROBE_ID>();
		auto traj_id = *(id.begin());
		InputVector input;
		auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
		auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
		auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
		auto it_lon = lons.begin();
		auto it_t = times.begin();
		for (auto it_lat = lats.begin(); it_lat != lats.end(); it_lat++)
		{
			auto tup = std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>(*it_lat, *it_lon, it_t->ts());
			input.push_back(tup);
			it_lon++;
			it_t++;
		}
		OutputVector result;
		timer.start();
		detect_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result));
		timer.stop();

		auto el = timer.elapsed();
		stringstream statsline;
		statsline << traj_id << ";" << id.size() << ";" << result.size() << ";" << el.wall << ";" << el.user << ";" << el.system << ";" << endl;
		stats.push_back(statsline.str());

		stringstream outliersline;
		outliersline << traj_id << ";" << (id.size() - result.size()) << ";";

		std::vector<ProbeTraits::ProbeCsv::value_type> filtered_points;
		auto tit = trajectory.begin();
		auto time_it = times.begin();
		if (result.size() == 0)
		{
			for (time_it; time_it != times.end(); time_it++)
				outliersline << *time_it << ";";
		}
		else {
			auto result_it = result.begin();
			for (auto it = input.begin(); it != input.end(); it++)
			{
				if (it == *result_it)
				{
					filtered_points.push_back(*tit);
					result_it++;
				}
				else
				{
					outliersline << *time_it << ";";
				}
				tit++;
				time_it++;
			}
		}
		outliersline << endl;
		outliers.push_back(outliersline.str());


		TrajectoryType filtered_trajectory(filtered_points);

		stringstream filename;
		if (gencsvs)
		{
			// Create an output csv file

			filename << outfilename << "outcsvs/" << traj_id << ".csv";
			std::ofstream ofcsv(filename.str());
			ofcsv << filtered_trajectory;
			stringstream().swap(filename);
		}
		if (genjsons)
		{
			if (result.size() != 0)
			{
				stringstream().swap(filename);
				filename << outfilename << "outjsons/" << traj_id << ".geojson";
				// Create a GeoJSON for easy visualisation
				std::ofstream ofjson(filename.str());
				ofjson.setf(std::ios::fixed);
				vector<TrajectoryTraits::trajectory_type> jsonvector;
				jsonvector.push_back(filtered_trajectory);
				tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
					ProbeTraits::ProbeColumns::LAT,
					ProbeTraits::ProbeColumns::LON>(ofjson,
						jsonvector.begin(),
						std::prev(jsonvector.end()));

				
			}
		}
		if (result.size() != 0)
			filtered_trajectories.push_back(filtered_trajectory);
	}

	stringstream statsfilename;
	statsfilename << outfilename << "stats.csv";
	ofstream statscsv(statsfilename.str());
	for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
		statscsv << *stats_it;
	stringstream outliersfilename;
	outliersfilename << outfilename << "outliers.csv";
	ofstream outlierscsv(outliersfilename.str());
	for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
		outlierscsv << *outliers_it;

	cout << "Experiment Completed" << endl;
}

template <class GeometryTraits, class TrajectoryTraits, class NT, class TrajectoryType>
void GreedyExperiment(std::vector<TrajectoryType> trajectories, NT maxspeed, string const&outfilename, std::vector<TrajectoryType> &filtered_trajectories, bool gencsvs = false, bool genjsons = false)
{
	vector<string> stats;
	stringstream header;
	header << "TRAJECTORY_ID;NUM_PROBES_IN;NUM_PROBES_OUT;TIME_WALL;TIME_USER;TIME_SYS;" << endl;
	stats.push_back(header.str());
	vector<string> outliers;
	stringstream().swap(header);
	header << "TRAJECTORY_ID;NUM_OUTLIERS;OUTLIER0;OUTLIER1;...;" << endl;
	outliers.push_back(header.str());

	using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
	cout << "Running experiment" << endl;
	size_t trajcount = trajectories.size();
	cout << "Filtering " << trajcount << " trajectories" << endl;
	tulib_algorithms::GreedyPOD<GeometryTraits, TrajectoryTraits, tulib_algorithms::DistanceOverTimeCheck<GeometryTraits>> detect_outliers(maxspeed);
	boost::timer::cpu_timer timer;

	typedef std::vector<std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>> InputVector;
	typedef std::vector<InputVector::iterator> OutputVector;
	size_t current_traj_index = 1;
	for (auto trajectory : trajectories)
	{
		cout << "Processing trajectory " << current_traj_index << " of " << trajcount << endl;
		current_traj_index++;

		auto id = trajectory.get<ProbeTraits::ProbeColumns::PROBE_ID>();
		auto traj_id = *(id.begin());
		InputVector input;
		auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
		auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
		auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
		auto it_lon = lons.begin();
		auto it_t = times.begin();
		for (auto it_lat = lats.begin(); it_lat != lats.end(); it_lat++)
		{
			auto tup = std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>(*it_lat, *it_lon, it_t->ts());
			input.push_back(tup);
			it_lon++;
			it_t++;
		}
		OutputVector result;
		timer.start();
		detect_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result));
		timer.stop();

		auto el = timer.elapsed();
		stringstream statsline;
		statsline << traj_id << ";" << id.size() << ";" << result.size() << ";" << el.wall << ";" << el.user << ";" << el.system << ";" << endl;
		stats.push_back(statsline.str());

		stringstream outliersline;
		outliersline << traj_id << ";" << (id.size() - result.size()) << ";";

		std::vector<ProbeTraits::ProbeCsv::value_type> filtered_points;
		auto tit = trajectory.begin();
		auto time_it = times.begin();
		auto result_it = result.begin();
		for (auto it = input.begin(); it != input.end(); it++)
		{
			if (it == *result_it)
			{
				filtered_points.push_back(*tit);
				result_it++;
			}
			else
			{
				outliersline << *time_it << ";";
			}
			tit++;
			time_it++;
		}
		outliersline << endl;
		outliers.push_back(outliersline.str());


		TrajectoryType filtered_trajectory(filtered_points);

		stringstream filename;
		if (gencsvs)
		{
			// Create an output csv file

			filename << outfilename << "outcsvs/" << traj_id << ".csv";
			std::ofstream ofcsv(filename.str());
			ofcsv << filtered_trajectory;
			stringstream().swap(filename);
		}
		if (genjsons)
		{
			filename << outfilename << "outjsons/" << traj_id << ".geojson";
			// Create a GeoJSON for easy visualisation
			std::ofstream ofjson(filename.str());
			ofjson.setf(std::ios::fixed);
			vector<TrajectoryTraits::trajectory_type> jsonvector;
			jsonvector.push_back(filtered_trajectory);
			tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
				ProbeTraits::ProbeColumns::LAT,
				ProbeTraits::ProbeColumns::LON>(ofjson,
					jsonvector.begin(),
					std::prev(jsonvector.end()));
		}

		filtered_trajectories.push_back(filtered_trajectory);
	}

	stringstream statsfilename;
	statsfilename << outfilename << "stats.csv";
	ofstream statscsv(statsfilename.str());
	for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
		statscsv << *stats_it;
	stringstream outliersfilename;
	outliersfilename << outfilename << "outliers.csv";
	ofstream outlierscsv(outliersfilename.str());
	for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
		outlierscsv << *outliers_it;

	cout << "Experiment Completed" << endl;
}

template <class GeometryTraits, class TrajectoryTraits, class NT, class TrajectoryType>
void SmartGreedyExperiment(std::vector<TrajectoryType> trajectories, NT maxspeed, string const&outfilename, std::vector<TrajectoryType> &filtered_trajectories, bool gencsvs = false, bool genjsons = false)
{
	vector<string> stats;
	stringstream header;
	header << "TRAJECTORY_ID;NUM_PROBES_IN;NUM_PROBES_OUT;TIME_WALL;TIME_USER;TIME_SYS;" << endl;
	stats.push_back(header.str());
	vector<string> outliers;
	stringstream().swap(header);
	header << "TRAJECTORY_ID;NUM_OUTLIERS;OUTLIER0;OUTLIER1;...;" << endl;
	outliers.push_back(header.str());

	using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
	cout << "Running experiment" << endl;
	size_t trajcount = trajectories.size();
	cout << "Filtering " << trajcount << " trajectories" << endl;
	tulib_algorithms::SmartGreedyPOD<GeometryTraits, TrajectoryTraits, tulib_algorithms::DistanceOverTimeCheck<GeometryTraits>> detect_outliers(maxspeed);
	boost::timer::cpu_timer timer;

	typedef std::vector<std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>> InputVector;
	typedef std::vector<InputVector::iterator> OutputVector;
	size_t current_traj_index = 1;
	for (auto trajectory : trajectories)
	{
		cout << "Processing trajectory " << current_traj_index << " of " << trajcount << endl;
		current_traj_index++;

		auto id = trajectory.get<ProbeTraits::ProbeColumns::PROBE_ID>();
		auto traj_id = *(id.begin());
		InputVector input;
		auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
		auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
		auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
		auto it_lon = lons.begin();
		auto it_t = times.begin();
		for (auto it_lat = lats.begin(); it_lat != lats.end(); it_lat++)
		{
			auto tup = std::tuple<GeometryTraits::NT, GeometryTraits::NT, time_t>(*it_lat, *it_lon, it_t->ts());
			input.push_back(tup);
			it_lon++;
			it_t++;
		}
		OutputVector result;
		timer.start();
		detect_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result));
		timer.stop();

		auto el = timer.elapsed();
		stringstream statsline;
		statsline << traj_id << ";" << id.size() << ";" << result.size() << ";" << el.wall << ";" << el.user << ";" << el.system << ";" << endl;
		stats.push_back(statsline.str());

		stringstream outliersline;
		outliersline << traj_id << ";" << (id.size() - result.size()) << ";";

		std::vector<ProbeTraits::ProbeCsv::value_type> filtered_points;
		auto tit = trajectory.begin();
		auto time_it = times.begin();
		auto result_it = result.begin();
		for (auto it = input.begin(); it != input.end(); it++)
		{
			if (it == *result_it)
			{
				filtered_points.push_back(*tit);
				result_it++;
			}
			else
			{
				outliersline << *time_it << ";";
			}
			tit++;
			time_it++;
		}
		outliersline << endl;
		outliers.push_back(outliersline.str());


		TrajectoryType filtered_trajectory(filtered_points);

		stringstream filename;
		if (gencsvs)
		{
			// Create an output csv file

			filename << outfilename << "outcsvs/" << traj_id << ".csv";
			std::ofstream ofcsv(filename.str());
			ofcsv << filtered_trajectory;
			stringstream().swap(filename);
		}
		if (genjsons)
		{
			filename << outfilename << "outjsons/" << traj_id << ".geojson";
			// Create a GeoJSON for easy visualisation
			std::ofstream ofjson(filename.str());
			ofjson.setf(std::ios::fixed);
			vector<TrajectoryTraits::trajectory_type> jsonvector;
			jsonvector.push_back(filtered_trajectory);
			tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
				ProbeTraits::ProbeColumns::LAT,
				ProbeTraits::ProbeColumns::LON>(ofjson,
					jsonvector.begin(),
					std::prev(jsonvector.end()));
		}

		filtered_trajectories.push_back(filtered_trajectory);
	}

	stringstream statsfilename;
	statsfilename << outfilename << "stats.csv";
	ofstream statscsv(statsfilename.str());
	for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
		statscsv << *stats_it;
	stringstream outliersfilename;
	outliersfilename << outfilename << "outliers.csv";
	ofstream outlierscsv(outliersfilename.str());
	for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
		outlierscsv << *outliers_it;

	cout << "Experiment Completed" << endl;
}

template <class GeometryTraits, class TrajectoryTraits, class TrajectoryType, class NT>
void NontransitiveGreedyExperiment(std::vector<TrajectoryType> trajectories, NT minspeed, NT maxspeed, NT minacc, NT maxacc, NT detourfactor, string const &outfilename, std::vector<TrajectoryType> &filtered_trajectories, bool gencsvs = false, bool genjsons = false)
{
	using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
	SpeedIntervalTraits<NT> SITraits(minspeed, maxspeed, minacc, maxacc, detourfactor);
	tulib_algorithms::NontransitiveGreedyPOD<GeometryTraits, SpeedIntervalTraits<NT>> detect_cis_outliers(SITraits);
	typedef std::vector<std::tuple<NT, NT, time_t>> NontransitiveInputVector;
	typedef std::vector<NontransitiveInputVector::iterator> NontransitiveOutputVector;
	std::vector<TrajectoryType> nontransitive_filtered_trajectories;

	cout << "Running experiment" << endl;
	size_t trajcount = trajectories.size();
	cout << "Filtering " << trajcount << " trajectories" << endl;
	boost::timer::cpu_timer timer;
	//Store our outputs until they are written to file
	vector<string> stats;
	stringstream header;
	header << "TRAJECTORY_ID;NUM_PROBES_IN;NUM_PROBES_OUT;TIME_WALL;TIME_USER;TIME_SYS;" << endl;
	stats.push_back(header.str());
	vector<string> outliers;
	stringstream().swap(header);
	header << "TRAJECTORY_ID;NUM_OUTLIERS;OUTLIER0;OUTLIER1;...;" << endl;
	outliers.push_back(header.str());

	size_t current_traj_index = 1;

	for (auto trajectory : trajectories)
	{
		cout << "Processing trajectory " << current_traj_index << " of " << trajcount << endl;
		current_traj_index++;
		NontransitiveInputVector input;

		auto id = trajectory.get<ProbeTraits::ProbeColumns::PROBE_ID>();
		auto traj_id = *id.begin();

		auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
		auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
		auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
		auto it_t = times.begin();
		auto it_lon = lons.begin();
		for (auto it_lat = lats.begin(); it_lat != lats.end(); it_lat++)
		{
			input.push_back(tuple<NT, NT, time_t>(*it_lat, *it_lon, it_t->ts()));
			it_t++;
			it_lon++;
		}

		NontransitiveOutputVector result;

		timer.start();
		detect_cis_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result));
		timer.stop();
		
		auto el = timer.elapsed();
		stringstream statsline;
		statsline << traj_id << ";" << id.size() << ";" << result.size() << ";" << el.wall << ";" << el.user << ";" << el.system << ";"  << endl;
		stats.push_back(statsline.str());

		stringstream outliersline;
		outliersline << traj_id << ";" << (id.size() - result.size()) << ";";
		//Match the returned indices back to the full probes
		auto tit = trajectory.begin();
		std::vector<ProbeTraits::ProbeCsv::value_type> filtered_points;
		auto time_it = times.begin();
		auto result_it = result.begin();
		for (auto it = input.begin(); it != input.end(); it++)
		{
			if (it == *result_it) //Because of backtracing in the algorithm, the output is processed back to front.
			{
				filtered_points.push_back(*tit);
				result_it++;
			}
			else
			{
				outliersline << *time_it << ";";
			}
			tit++;
			time_it++;
		}
		outliersline << endl;
		outliers.push_back(outliersline.str());
		TrajectoryType filtered_trajectory(filtered_points);

		stringstream filename;
		if (gencsvs)
		{
			// Create an output csv file

			filename << outfilename << "outcsvs/" << traj_id << ".csv";
			std::ofstream ofcsv(filename.str());
			ofcsv << filtered_trajectory;
			stringstream().swap(filename);
		}
		if (genjsons)
		{
			filename << outfilename << "outjsons/" << traj_id << ".geojson";
			// Create a GeoJSON for easy visualisation
			std::ofstream ofjson(filename.str());
			ofjson.setf(std::ios::fixed);
			vector<TrajectoryTraits::trajectory_type> jsonvector;
			jsonvector.push_back(filtered_trajectory);
			tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
				ProbeTraits::ProbeColumns::LAT,
				ProbeTraits::ProbeColumns::LON>(ofjson,
					jsonvector.begin(),
					std::prev(jsonvector.end()));
		}

		filtered_trajectories.push_back(filtered_trajectory);
	}

	stringstream statsfilename;
	statsfilename << outfilename << "stats.csv";
	ofstream statscsv(statsfilename.str());
	for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
		statscsv << *stats_it;
	stringstream outliersfilename;
	outliersfilename << outfilename << "outliers.csv";
	ofstream outlierscsv(outliersfilename.str());
	for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
		outlierscsv << *outliers_it;
}

template <class GeometryTraits, class TrajectoryTraits, class TrajectoryType, class NT>
void NontransitiveSmartGreedyExperiment(std::vector<TrajectoryType> trajectories, NT minspeed, NT maxspeed, NT minacc, NT maxacc, NT detourfactor, string const &outfilename, std::vector<TrajectoryType> &filtered_trajectories, bool gencsvs = false, bool genjsons = false)
{
	using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
	SpeedIntervalTraits<NT> SITraits(minspeed, maxspeed, minacc, maxacc, detourfactor);
	tulib_algorithms::NontransitiveSmartGreedyPOD<GeometryTraits, SpeedIntervalTraits<NT>> detect_cis_outliers(SITraits);
	typedef std::vector<std::tuple<NT, NT, time_t>> NontransitiveInputVector;
	typedef std::vector<NontransitiveInputVector::iterator> NontransitiveOutputVector;
	std::vector<TrajectoryType> nontransitive_filtered_trajectories;

	cout << "Running experiment" << endl;
	size_t trajcount = trajectories.size();
	cout << "Filtering " << trajcount << " trajectories" << endl;
	boost::timer::cpu_timer timer;
	//Store our outputs until they are written to file
	vector<string> stats;
	stringstream header;
	header << "TRAJECTORY_ID;NUM_PROBES_IN;NUM_PROBES_OUT;TIME_WALL;TIME_USER;TIME_SYS;" << endl;
	stats.push_back(header.str());
	vector<string> outliers;
	stringstream().swap(header);
	header << "TRAJECTORY_ID;NUM_OUTLIERS;OUTLIER0;OUTLIER1;...;" << endl;
	outliers.push_back(header.str());

	size_t current_traj_index = 1;

	for (auto trajectory : trajectories)
	{
		cout << "Processing trajectory " << current_traj_index << " of " << trajcount << endl;
		current_traj_index++;
		NontransitiveInputVector input;

		auto id = trajectory.get<ProbeTraits::ProbeColumns::PROBE_ID>();
		auto traj_id = *id.begin();

		auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
		auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
		auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
		auto it_t = times.begin();
		auto it_lon = lons.begin();
		for (auto it_lat = lats.begin(); it_lat != lats.end(); it_lat++)
		{
			input.push_back(tuple<NT, NT, time_t>(*it_lat, *it_lon, it_t->ts()));
			it_t++;
			it_lon++;
		}

		NontransitiveOutputVector result;

		timer.start();
		detect_cis_outliers(input.begin(), input.end(), tulib_core::tulib_back_insert_iterator(result));
		timer.stop();

		auto el = timer.elapsed();
		stringstream statsline;
		statsline << traj_id << ";" << id.size() << ";" << result.size() << ";" << el.wall << ";" << el.user << ";" << el.system << ";" << endl;
		stats.push_back(statsline.str());

		stringstream outliersline;
		outliersline << traj_id << ";" << (id.size() - result.size()) << ";";
		//Match the returned indices back to the full probes
		auto tit = trajectory.begin();
		std::vector<ProbeTraits::ProbeCsv::value_type> filtered_points;
		auto time_it = times.begin();
		auto result_it = result.begin();
		for (auto it = input.begin(); it != input.end(); it++)
		{
			if (it == *result_it)
			{
				filtered_points.push_back(*tit);
				result_it++;
			}
			else
			{
				outliersline << *time_it << ";";
			}
			tit++;
			time_it++;
		}
		outliersline << endl;
		outliers.push_back(outliersline.str());
		TrajectoryType filtered_trajectory(filtered_points);

		stringstream filename;
		if (gencsvs)
		{
			// Create an output csv file

			filename << outfilename << "outcsvs/" << traj_id << ".csv";
			std::ofstream ofcsv(filename.str());
			ofcsv << filtered_trajectory;
			stringstream().swap(filename);
		}
		if (genjsons)
		{
			filename << outfilename << "outjsons/" << traj_id << ".geojson";
			// Create a GeoJSON for easy visualisation
			std::ofstream ofjson(filename.str());
			ofjson.setf(std::ios::fixed);
			vector<TrajectoryTraits::trajectory_type> jsonvector;
			jsonvector.push_back(filtered_trajectory);
			tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
				ProbeTraits::ProbeColumns::LAT,
				ProbeTraits::ProbeColumns::LON>(ofjson,
					jsonvector.begin(),
					std::prev(jsonvector.end()));
		}

		filtered_trajectories.push_back(filtered_trajectory);
	}

	stringstream statsfilename;
	statsfilename << outfilename << "stats.csv";
	ofstream statscsv(statsfilename.str());
	for (auto stats_it = stats.begin(); stats_it != stats.end(); stats_it++)
		statscsv << *stats_it;
	stringstream outliersfilename;
	outliersfilename << outfilename << "outliers.csv";
	ofstream outlierscsv(outliersfilename.str());
	for (auto outliers_it = outliers.begin(); outliers_it != outliers.end(); outliers_it++)
		outlierscsv << *outliers_it;
}

//template <class GeometryTraits, class TrajectoryTraits, class NT, class TrajectoryType>
//void AnalyzeTrajectories(std::vector<TrajectoryType> trajectories, string const&outfilename, std::vector<TrajectoryType> &filtered_trajectories)
//{
//	using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
//	map<int, size_t> speedfrequencies;
//	map<int, size_t> accfrequencies;
//	map<int, size_t> calcspeedfrequencies;
//	map<int, size_t> calcaccfrequencies;
//	NT min_speed = 1000000;
//	NT max_speed = -1000000;
//	NT min_acc = 1000000;
//	NT max_acc = -1000000;
//	NT max_calc_speed = -10000000;
//	NT min_calc_speed = 1000000;
//	NT min_calc_acc = 10000000;
//	NT max_calc_acc = -10000000;
//	for (auto trajectory : trajectories)
//	{
//		filtered_trajectories.push_back(trajectory);
//		auto speeds = trajectory.get<ProbeTraits::ProbeColumns::SPEED>();
//		auto times = trajectory.get<ProbeTraits::ProbeColumns::SAMPLE_DATE>();
//		auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
//		auto lons = trajectory.get<ProbeTraits::ProbeColumns::LON>();
//		auto it_speed = speeds.begin();
//		auto it_time = times.begin();
//		auto it_lat = lats.begin();
//		auto it_lon = lons.begin();
//		NT prev_speed = *it_speed;
//		NT prev_time = it_time->ts();
//		NT prev_lat = *it_lat;
//		NT prev_lon = *it_lon;
//		NT prev_calc_speed = prev_speed;
//		it_speed++;
//		it_time++;
//		it_lat++;
//		it_lon++;
//		for (it_speed; it_speed != speeds.end(); it_speed++)
//		{
//
//			NT distance = distance_exact(prev_lat, prev_lon, *it_lat, *it_lon);
//			NT speed_diff = *it_speed - prev_speed;
//			NT time_diff = it_time->ts() - prev_time;
//			NT calc_speed = distance / time_diff;
//			NT calc_speed_diff = calc_speed - prev_calc_speed;
//			NT acc = speed_diff / time_diff;
//			NT calc_acc = calc_speed_diff / time_diff;
//
//			if (*it_speed > max_speed)
//				max_speed = *it_speed;
//			if (*it_speed < min_speed)
//				min_speed = *it_speed;
//			if (calc_speed > max_calc_speed)
//				max_calc_speed = calc_speed;
//			if (calc_speed < min_calc_speed)
//				min_calc_speed = calc_speed;
//			if (acc > max_acc)
//				max_acc = acc;
//			if (acc < min_acc)
//				min_acc = acc;
//			if (calc_acc > max_calc_acc)
//				max_calc_acc = calc_acc;
//			if (calc_acc < min_calc_acc)
//				min_calc_acc = calc_acc;
//
//			int intspeed = (int)*it_speed;
//			if (speedfrequencies.count(intspeed) == 1)
//				speedfrequencies[intspeed] = speedfrequencies[intspeed] + 1;
//			else speedfrequencies[intspeed] = 1;
//			int intcalcspeed = (int)calc_speed;
//			if (calcspeedfrequencies.count(intcalcspeed) == 1)
//				calcspeedfrequencies[intcalcspeed] = calcspeedfrequencies[intcalcspeed] + 1;
//			else calcspeedfrequencies[intcalcspeed] = 1;
//			int intacc = (int)acc;
//			if (accfrequencies.count(intacc) == 1)
//				accfrequencies[intacc] = accfrequencies[intacc] + 1;
//			else accfrequencies[intacc] = 1;
//			int intcalcacc = (int)calc_acc;
//			if (calcaccfrequencies.count(intcalcacc) == 1)
//				calcaccfrequencies[intcalcacc] = calcaccfrequencies[intcalcacc] + 1;
//			else calcaccfrequencies[intcalcacc] = 1;
//
//			prev_speed = *it_speed;
//			prev_time = it_time->ts();
//			prev_calc_speed = calc_speed;
//			prev_lat = *it_lat;
//			prev_lon = *it_lon;
//
//			it_time++;
//			it_lat++;
//			it_lon++;
//		}
//	}
//	ofstream outfile;
//	outfile.open(outfilename);
//	outfile << "File Analysis" << endl;
//	outfile << "NUMTRAJECTORIES" << endl << trajectories.size() << endl;
//	outfile << "MINSPEED" << endl << min_speed << endl;
//	outfile << "MAXSPEED" << endl << max_speed << endl;
//	outfile << "MINCALCSPEED" << endl << min_calc_speed << endl;
//	outfile << "MAXCALCSPEED" << endl << max_calc_speed << endl;
//	outfile << "MINACC" << endl << min_acc << endl;
//	outfile << "MAXACC" << endl << max_acc << endl;
//	outfile << "MINCALCACC" << endl << min_calc_acc << endl;
//	outfile << "MAXCALCACC" << endl << max_calc_acc << endl;
//	outfile << "SPEEDFREQUENCIES" << endl;
//	for (auto& x : speedfrequencies)
//	{
//		outfile << x.first << " " << x.second << std::endl;
//	}
//	outfile << endl;
//	outfile << "CALCSPEEDFREQUENCIES" << endl;
//	for (auto& x : calcspeedfrequencies)
//	{
//		outfile << x.first << " " << x.second << std::endl;
//	}
//	outfile << endl;
//	outfile << "ACCFREQUENCIES" << endl;
//	for (auto& x : accfrequencies)
//	{
//		outfile << x.first << " " << x.second << std::endl;
//	}
//	outfile << endl;
//	outfile << "CALCACCFREQUENCIES" << endl;
//	for (auto& x : calcaccfrequencies)
//	{
//		outfile << x.first << " " << x.second << std::endl;
//	}
//	outfile << endl;
//
//	outfile.close();
//}

int main(int argc, char** argv)
{
	if (argc < 2) //GUI mode
	{
//		QApplication app(argc, argv);
//		MainWindow window;
//		window.setGeometry(0, 0, 800, 800);
//		window.show();
//		app.exec();
	}
	else
	{
		if (argc != 11)
		{
			cout << "Not all arguments have been supplied. For GUI mode, run the program without any arguments" << endl;
			cout << "Supplied arguments: " << endl;
			for (int i = 0; i < argc; i++)
				cout << "argv[" << i << "] = " << argv[i] << endl;
			string foo;
			getline(std::cin, foo);
			cout << foo;
			return 0;
		}
		// Specializations for the Commit2Data raw probe format
		using TrajectoryTraits = here::c2d::raw::TabularTrajectoryTraits;
		using ProbeTraits = typename TrajectoryTraits::ProbeTraits;
		using GeometryTraits = GeometryKernel::TulibGeometryKernel;
		typedef GeometryTraits::NT NT;
		// Create trajectory reader
		std::unique_ptr<ProbeReader<ProbeTraits>> probe_reader;
		//ProbeTraits::ProbeParseDate dateparser;
		//std::get_date
		//dateparser("10-1-2019 19:02");
		//Read in the trajectories
		auto infile = argv[1];
		cout << "Attempting to load file " << infile << endl;
		try
		{
			probe_reader = ProbeReaderFactory::create<ProbeTraits>(infile);
		}
		catch (...)
		{
			cout << "Unable to load file " << infile << endl;
			return 0;
		}
		auto outfile = argv[2];
		using ProbeInputIterator = decltype(probe_reader->begin());
		constexpr int PROBE_ID = ProbeTraits::ProbeColumns::PROBE_ID;
		SortedProbeReader<ProbeInputIterator, PROBE_ID> sorted_probe_reader(probe_reader->begin(), probe_reader->end());
		using SortedProbeInputIterator = decltype(sorted_probe_reader.begin());
		auto trajectory_reader = TrajectoryReader<TrajectoryTraits, SortedProbeInputIterator>(sorted_probe_reader.begin(), sorted_probe_reader.end());
		vector<TrajectoryTraits::trajectory_type> trajectories;
		auto min_trajectory_length = stold(argv[10]);
//		NT max_speed, min_speed, min_acc, max_acc;
		//size_t current_traj_index = 1;
		for (auto trajectory : trajectory_reader)
		{
			auto lats = trajectory.get<ProbeTraits::ProbeColumns::LAT>();
			auto ids = trajectory.get<ProbeTraits::ProbeColumns::PROBE_ID>();
			//if (ids.front() == "00074BB0-A3E0-4104-A103-0430737828CC")
			//{
			//	ofstream offcsv("weirdtraj.csv");
			//	offcsv << trajectory;
			//	offcsv.close();
			//	return 0;
			//}
			if (lats.size() >= min_trajectory_length)
			{
				trajectories.push_back(trajectory);
			}
			//if (trajectories.size() == 3)
			//{
			//	ofstream ofssd("errtraj.csv");
			//	ofssd << trajectory;
			//	return 0;
			//}
		}
		cout << "Succesfully loaded trajectories" << endl;
		//ofstream output;
		//output.open((string)outfile + ".txt");
		auto algname = argv[3];
		auto filtered_trajectories = vector<TrajectoryTraits::trajectory_type>();
		auto outfilename = (string)outfile;
		//vector<NT> maxspeeds;
		//for (int i = 0; i < 11; i++)
		//	maxspeeds.push_back((long double)(i * 2 + 60) / 3.6);
		//maxspeeds.push_back(22.22);
		//maxspeeds.push_back(23.61);
		//maxspeeds.push_back(25);
		//maxspeeds.push_back(26.39);
		//maxspeeds.push_back(27.78);
		//maxspeeds.push_back(29.17);
		//maxspeeds.push_back(30.56);
		//maxspeeds.push_back(31.33);
		//maxspeeds.push_back(33.33);
		//maxspeeds.push_back(34.72);
		//maxspeeds.push_back(36.11);
		//maxspeeds.push_back(37.5);
		//maxspeeds.push_back(38.89);
		//maxspeeds.push_back(40.28);
		//maxspeeds.push_back(41.67);
		//maxspeeds.push_back(43.06);
		//maxspeeds.push_back(44.44);

		//for (int i = 0; i < 17; i++)
		//{
			//stringstream foo;
			//foo << outfile << i << "\\";
			//auto outfilename = foo.str();
			//NT the_maxspeed = maxspeeds[i];
			NT the_maxspeed = stold(argv[5]);
			switch (stoi(algname))
			{
			case 0:
				OutputSensitiveExperiment<GeometryTraits, TrajectoryTraits, GeometryTraits::NT, TrajectoryTraits::trajectory_type>(trajectories, the_maxspeed, outfilename, filtered_trajectories);
				break;
			case 1:
				NontransitiveExperiment<GeometryTraits, TrajectoryTraits, TrajectoryTraits::trajectory_type, GeometryTraits::NT>(trajectories, stold(argv[4]), the_maxspeed, stold(argv[6]), stold(argv[7]), stold(argv[8]), outfilename, filtered_trajectories);
				break;
			case 2:
				ZhengGreedyExperiment<GeometryTraits, TrajectoryTraits, GeometryTraits::NT, TrajectoryTraits::trajectory_type>(trajectories, the_maxspeed, stold(argv[9]), outfilename, filtered_trajectories);
				break;
			case 3:
				GreedyExperiment<GeometryTraits, TrajectoryTraits, GeometryTraits::NT, TrajectoryTraits::trajectory_type>(trajectories, the_maxspeed, outfilename, filtered_trajectories);
				break;
			case 4:
				SmartGreedyExperiment<GeometryTraits, TrajectoryTraits, GeometryTraits::NT, TrajectoryTraits::trajectory_type>(trajectories, the_maxspeed, outfilename, filtered_trajectories);
				break;
			case 5:
				NontransitiveGreedyExperiment<GeometryTraits, TrajectoryTraits, TrajectoryTraits::trajectory_type, GeometryTraits::NT>(trajectories, stold(argv[4]), the_maxspeed, stold(argv[6]), stold(argv[7]), stold(argv[8]), outfilename, filtered_trajectories);
				break;
			case 6:
				NontransitiveSmartGreedyExperiment<GeometryTraits, TrajectoryTraits, TrajectoryTraits::trajectory_type, GeometryTraits::NT>(trajectories, stold(argv[4]), the_maxspeed, stold(argv[6]), stold(argv[7]), stold(argv[8]), outfilename, filtered_trajectories);
				break;
				//case 9001:
				//	AnalyzeTrajectories<GeometryTraits, TrajectoryTraits, GeometryTraits::NT, TrajectoryTraits::trajectory_type>(trajectories, outfilename, filtered_trajectories);
				//	break;
			}
		//}

		 //Create an output csv file
		std::ofstream ofcsv((string)outfile + ".csv");
		for (auto cit = filtered_trajectories.begin(); cit != filtered_trajectories.end(); cit++)
			ofcsv << *cit;

		//// Create a GeoJSON for easy visualisation
		//std::ofstream ofjson((string)outfile+".geojson");
		//ofjson.setf(std::ios::fixed);
		//tulib_io::trajectories_to_geojson<typename std::vector<TrajectoryTraits::trajectory_type>::iterator,
		//	ProbeTraits::ProbeColumns::LAT,
		//	ProbeTraits::ProbeColumns::LON>(ofjson,
		//		filtered_trajectories.begin(),
		//		std::prev(filtered_trajectories.end()));
	}
}

