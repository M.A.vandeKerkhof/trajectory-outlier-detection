#ifndef TULIB_NONTRANSITIVEGREEDYPOD_H
#define TULIB_NONTRANSITIVEGREEDYPOD_H

#include "tulib/utils/Requirements.h"
#include <algorithm>
#include <iostream>
#include <string>
namespace tulib_algorithms {
	template <class GeometryTraits, class IntervalTraits>
	class NontransitiveGreedyPOD {
	private:
		typedef typename GeometryTraits::NT NT;
		IntervalTraits traits;

	public:
		NontransitiveGreedyPOD(IntervalTraits InTraits) { traits = InTraits; }

		template <class InputIterator, class OutputIterator,
			typename = tulib_core::requires_random_access_iterator <InputIterator>,
			typename = tulib_core::requires_output_iterator <OutputIterator>,
			typename = tulib_core::requires_equality<typename InputIterator::value_type,
			typename OutputIterator::value_type::value_type >>
			void operator()(InputIterator first, InputIterator beyond, OutputIterator result)
		{
			map<tuple<__int64,__int64>,NT> distancemap;
			vector<IntervalTraits::Interval> currentIntervals;
			currentIntervals.push_back(traits.base_interval());
			auto traj_it = first;
			auto currentProbe = *traj_it;
			__int64 currentprobe_index = 0;
			*result = traj_it;
			traj_it++;
			for (traj_it; traj_it != beyond; traj_it++)
			{
				__int64 newprobe_index = traj_it - first;
				auto newprobe = *traj_it;
				if (newprobe_index == 34)
				{
					cout << "breakpoint " << endl;
				}
				auto newIntervals = traits.propagate(currentIntervals, currentProbe, newprobe, currentprobe_index,newprobe_index,distancemap);
				cout << newIntervals.size() << endl;
				if (newIntervals.size() != 0)
				{
					*result = traj_it;
					currentIntervals = newIntervals;
					currentProbe = newprobe;
					currentprobe_index = newprobe_index;
				}
				else 
				{
					cout << "breakpoint " << endl;
				}
			}
		}
	};
}

#endif //TULIB_NONTRANSITIVEGREEDYPOD_H

