#ifndef TULIB_GREEDYPOD_H
#define TULIB_GREEDYPOD_H

#include "tulib/utils/Requirements.h"

using namespace std;
/*!
 * @brief a collection of algorithms in tulib
 */
namespace tulib_algorithms
{
	/*!
	* @tparam GeometryTraits
	* @tparam TrajectoryTraits
	* @tparam TestType
	*/
	template <class GeometryTraits, class TrajectoryTraits, class TestType>
	class GreedyPOD {


	private:
		typedef typename GeometryTraits::NT NT;
		typedef TestType TEST;
		typedef typename GeometryTraits::Tulib_Point Point;
		typedef typename TrajectoryTraits::ProbeTraits::ProbeColumns Columns;
		TEST test;

		NT threshold;

	public:

		/*!
		*@param InThreshold
		*/
		GreedyPOD(NT InThreshold) { threshold = InThreshold; test = TEST(InThreshold);};

		/*!
 *
 * @tparam InputIterator
 * @tparam OutputIterator
 * @param first
 * @param beyond
 * @param result
 */
		template <class InputIterator, class OutputIterator,
			typename = tulib_core::requires_random_access_iterator <InputIterator>,
			typename = tulib_core::requires_output_iterator <OutputIterator>,
			typename = tulib_core::requires_equality<typename InputIterator::value_type,
			typename OutputIterator::value_type::value_type >>
			void operator()(InputIterator first, InputIterator beyond, OutputIterator result)
		{
			auto it = first;
			*result = it;
			auto prev = *it;
			it++;
			for (it; it != beyond; it++)
			{
				if (test(prev, *it))
				{
					*result = it;
					prev = *it;
				}
			}
		} //operator()
	}; //class GreedyPOD
}

#endif //TULIB_GREEDYPOD_H
