#ifndef TULIB_SMARTGREEDYPOD_H
#define TULIB_SMARTGREEDYPOD_H

#include "tulib/utils/Requirements.h"

using namespace std;
/*!
 * @brief a collection of algorithms in tulib
 */
namespace tulib_algorithms
{

	template <class Sizeable>
	struct IterateComparer
	{
		bool operator()	(Sizeable a, Sizeable b) {
			return a.size() < b.size();
		}
	};




	/*!
	* @tparam GeometryTraits
	* @tparam TrajectoryTraits
	* @tparam TestType
	*/
	template <class GeometryTraits, class TrajectoryTraits, class TestType>
	class SmartGreedyPOD {


	private:
		typedef typename GeometryTraits::NT NT;
		typedef TestType TEST;
		typedef typename GeometryTraits::Tulib_Point Point;
		typedef typename TrajectoryTraits::ProbeTraits::ProbeColumns Columns;
		TEST test;

		NT threshold;

	public:

		/*!
		*@param InThreshold
		*/
		SmartGreedyPOD(NT InThreshold) { threshold = InThreshold; test = TEST(InThreshold); };

		/*!
 *
 * @tparam InputIterator
 * @tparam OutputIterator
 * @param first
 * @param beyond
 * @param result
 */
		template <class InputIterator, class OutputIterator,
			typename = tulib_core::requires_random_access_iterator <InputIterator>,
			typename = tulib_core::requires_output_iterator <OutputIterator>,
			typename = tulib_core::requires_equality<typename InputIterator::value_type,
			typename OutputIterator::value_type::value_type >>
			void operator()(InputIterator first, InputIterator beyond, OutputIterator result)
		{
			vector<vector<InputIterator>> trajectories;
			vector<InputIterator> base;
			base.push_back(first);
			trajectories.push_back(base);
			auto it = first;
			for (it; it != beyond; it++)
			{
				bool contained = false;
				for (auto traj_it = trajectories.begin(); traj_it != trajectories.end(); traj_it++)
				{
					if (test(*(traj_it->back()), *it))
					{
						traj_it->push_back(it);
						contained = true;
					}
				}
				if (!contained)
				{
					vector<InputIterator> newtraj;
					newtraj.push_back(it);
					trajectories.push_back(newtraj);
				}
			}
			IterateComparer<vector<InputIterator>> comparer;
			vector<InputIterator> max = *(max_element(trajectories.begin(), trajectories.end(), comparer));
			auto it_m = max.begin();
			for (it_m; it_m != max.end(); it_m++)
			{
				*result = *it_m;
			}
		} //operator()
	}; //class SmartGreedyPOD
}

#endif //TULIB_SMARTGREEDYPOD_H
