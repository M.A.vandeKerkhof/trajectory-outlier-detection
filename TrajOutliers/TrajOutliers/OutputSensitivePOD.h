
/*! @file OutputSensitivePOD.h
 *  @brief  The Output Sensitive Physical Outlier Detection algorithm
 *  @authors Mees van de Kerkhof (m.a.vandekerkhof@uu.nl)
 */

/*This algorithm takes in a set of tests and finds a set of maximum consistent subset of the trajectory.
It makes the assumption that the collection of tests adds up to a transitive consistency relation.
If this assumption is violated the algorithm may produce incorrect results.*/


#ifndef OUTPUTSENSITIVEPOD_H
#define OUTPUTSENSITIVEPOD_H

#include "tulib/utils/Requirements.h"

using namespace std;
 /*!
  * @brief a collection of algorithms in tulib
  */
namespace tulib_algorithms
{
	/*!
	* @tparam GeometryTraits
	* @tparam TrajectoryTraits
	* @tparam TestType
	*/
	template <class GeometryTraits, class TrajectoryTraits, class TestType>
	class OutputSensitivePOD {


	private :
		typedef typename GeometryTraits::NT NT;
		typedef TestType TEST;
		typedef typename GeometryTraits::Tulib_Point Point;
		typedef typename TrajectoryTraits::ProbeTraits::ProbeColumns Columns;
		TEST test;

		NT threshold;

		/*!
		* @tparam InputIterator
		*/
		template <class InputIterator>
		struct LinkedListNode
		{
		public:
			InputIterator probe; //Pointer to the underlying probe. 
			size_t k; //Length of the largest consistent trajectory ending in this probe
			shared_ptr<LinkedListNode<InputIterator>> prev_probe; //Pointer to previous probe in Longest Consistent Trajectory, needed for backtracing

			/*!
			*@param probeptr
			*/
			LinkedListNode(InputIterator probeptr) {				
				probe = probeptr; 
			}
		};
	public:

		/*!
		*@param InThreshold
		*/
		OutputSensitivePOD(NT InThreshold) { threshold = InThreshold; test = TEST(InThreshold);};

		/*!
 *
 * @tparam InputIterator
 * @tparam OutputIterator
 * @param first
 * @param beyond
 * @param result
 */
		template <class InputIterator, class OutputIterator,
			typename = tulib_core::requires_random_access_iterator <InputIterator>,
			typename = tulib_core::requires_output_iterator <OutputIterator>,
			typename = tulib_core::requires_equality<typename InputIterator::value_type,
			typename OutputIterator::value_type::value_type >>
			void operator()(InputIterator first, InputIterator beyond, OutputIterator result) 
		{
			std::list<LinkedListNode<InputIterator>> ll = {}; //Create an empty linked list
			InputIterator it = first;
			while (it != beyond) //Loop over the probes and place them in the spot in the list
			{		

				auto lln = LinkedListNode<InputIterator>(it); //Make the new node
				bool done = false;
				auto topk = ll.begin(); //Track the place in the list the new node should be inserted
				for (auto lli = ll.begin(); lli != ll.end(); lli++) //Go over the list until we get a consistent probe
				{
					if (lli->k < topk->k)
					{
						topk = lli;
					}
					if (test(*(lli->probe),*(lln.probe)))
					{
						lln.k = lli->k + 1;
						lln.prev_probe = std::make_shared<LinkedListNode<InputIterator>>(*lli);
						done = true;
						ll.insert(topk, lln);
						break;
					}
				}
				if (!done)
				{
					lln.k = 1;
					lln.prev_probe = nullptr;
					ll.push_back(lln);
				}
				it++; //Update the iterator
			}
			//Use backtracking to construct our filtered trajectory
			if (ll.empty())
				return;
			*result = ll.front().probe;
			shared_ptr<LinkedListNode<InputIterator>> lli = std::make_shared<LinkedListNode<InputIterator>>(ll.front());
			while (lli->prev_probe != nullptr)
			{
				lli = lli->prev_probe;
				*result = lli->probe;
			}
		}
	};
}

#endif //OUTPUTSENSITIVEPOD_H
